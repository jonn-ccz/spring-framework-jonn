# spring-framework-jonn

#### 介绍
 - 自定义实现Spring-IOC框架
 - 实现自动bean装配功能以及bean之间的依赖关系
 - 提供了xml和注解混合方式,注解功能需要通过xml (<annotation-config basePackage="com.lagou.zcc.spring"/>)启用.
 - 提供了统一的事务管理接口-需要实现Transcation接口来自定义事务.
 - Aop采用 JDK代理和CGlIB动态代理技术可通过xml的 aop-config配置使用.
 - BeanFactory提供顶级接口功能.可自定义实现.
#### MAVEN配置
```xml
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
            <version>1.1.16</version>
        </dependency>
        <!--dom4j解析xml-->
        <dependency>
            <groupId>dom4j</groupId>
            <artifactId>dom4j</artifactId>
            <version>1.6.1</version>
        </dependency>
        <dependency>
            <groupId>jaxen</groupId>
            <artifactId>jaxen</artifactId>
            <version>1.1.6</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>1.7.26</version>
        </dependency>

        <dependency>
            <groupId>cn.hutool</groupId>
            <artifactId>hutool-all</artifactId>
            <version>5.0.0</version>
        </dependency>
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
            <version>2.6</version>
        </dependency>
        <!--引入cglib依赖包-->
        <dependency>
            <groupId>cglib</groupId>
            <artifactId>cglib</artifactId>
            <version>2.1_2</version>
        </dependency>

```


#### 使用说明
- lombok插件安装说明 : 自行百度.





