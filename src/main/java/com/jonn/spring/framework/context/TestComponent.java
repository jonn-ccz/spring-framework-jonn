package com.jonn.spring.framework.context;

import com.jonn.spring.framework.context.stereotype.Component;

/**
 * 下午4:56 2020/9/8
 * lagou-transfer
 * Author: jonn
 * Desc:
 */
@Component(value = "testComponent")
public class TestComponent {
}
