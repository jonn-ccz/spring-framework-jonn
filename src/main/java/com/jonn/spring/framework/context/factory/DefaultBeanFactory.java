package com.jonn.spring.framework.context.factory;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.BooleanUtil;
import com.jonn.spring.framework.Constants;
import com.jonn.spring.framework.context.bean.BeanDefinition;
import com.jonn.spring.framework.context.builder.XmlBeanBuilder;
import com.jonn.spring.framework.context.proxy.TranscationalProxyFactory;
import com.jonn.spring.framework.context.stereotype.Autowired;
import com.jonn.spring.framework.context.stereotype.Transcational;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.apache.commons.lang.BooleanUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

/**
 * 下午4:14 2020/8/31
 * lagou-web
 * Author: jonn
 * Title: 真正的大师都怀着一颗学徒的心.
 * Desc:
 */
@Slf4j
public class DefaultBeanFactory implements BeanFactory {


	/**
	 * 用来存放bean集合
	 **/
	private static Map<String, Object> beanMap = new ConcurrentHashMap<>();


	/**
	 * 用来存放class和beanName的对应关系
	 **/
	private static Map<String, BeanDefinition> beanDefinitionsMap = new ConcurrentHashMap<>();

	/**
	 * 是否采用CgLib代理
	 **/
	private Boolean CGLIB_PROXY = false;

	/**
	 * 是否刷新
	 **/
	private Boolean refresh = true;

	private final String resourceName = "applicationContext.xml";

	public DefaultBeanFactory() {
		if (refresh) {
			//防止bean工厂实例化
			this.refresh = false;
			initBean();
		}
	}

	/**
	 * 初始化并装载Bean.
	 */
	private void initBean() {
		try {
			//解析bean
			log.info("begin init bean ");
			processBeanBuilder();


			//维护bean关系
			log.info("begin init bean  Of fixBeanDependOnBeanFactory");
			fixBeanDependOnBeanFactory();


			//处理事务代理对象
			log.info("begin init bean  Of fixBeanTranProxyBeanFactory");
			fixBeanTranProxyBeanFactory();

			log.info("init bean success: " + beanMap);
		} catch (DocumentException e) {
			log.error(e.getMessage(), e);
		}
	}

	private void processBeanBuilder() throws DocumentException {
		InputStream inputStream = DefaultBeanFactory.class.getClassLoader().getResourceAsStream(resourceName);
		SAXReader reader = new SAXReader();
		Document document = reader.read(inputStream);
		Element root = document.getRootElement();
		//是否开启了注解扫描
		Element configElement = root.element("annotation-config");
		//是否强制使用CGLIB代理对象
		Element aopConfigElement = root.element("aop-config");
		//扫描注解的bean
		if (configElement != null) {
			AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(this, configElement.attributeValue("basePackage"));
		}
		if (aopConfigElement != null && BooleanUtils.toBoolean(aopConfigElement.attributeValue("proxy-target-class"))) {
			CGLIB_PROXY = true;
		}
		//XML解析
		XmlBeanBuilder xmlBeanBuilder = new XmlBeanBuilder(beanMap);
		xmlBeanBuilder.parseXMLBean(root);
	}

	/**
	 * 处理事务代理对象.
	 */
	private void fixBeanTranProxyBeanFactory() {
		getBeanDefinitionsMap().forEach((beanId, beanDefinition) -> {
			if (beanDefinition.getAnnotation() != null && beanDefinition.getAnnotation().getName().equals(Transcational.class.getName())) {
				Object proxy = createTranProxy(getBeanMap().get(beanId));
				getBeanMap().put(beanDefinition.getBeanId(), proxy);
			}
		});

	}

	/**
	 * 创建事务的代理对象.
	 *
	 * @param target
	 * @return
	 */
	private Object createTranProxy(Object target) {
		//是否实现了自定义事务接口
		Transaction transaction = getBean(Transaction.class);
		Assert.notNull(transaction, "the transaction is not found in container");
		TranscationalProxyFactory transcationalProxyFactory = new TranscationalProxyFactory(transaction, target);
		if (CGLIB_PROXY && !target.getClass().isInterface() && target.getClass().getInterfaces().length == 0) {
			return transcationalProxyFactory.createCglibProxy();
		} else {
			return transcationalProxyFactory.createJDKProxy();
		}
	}


	/**
	 * 维护bean工厂的依赖关系.
	 */
	private void fixBeanDependOnBeanFactory() {
		for (Map.Entry<String, Object> entry : getBeanMap().entrySet()) {
			String beanId = entry.getKey();
			Object bean = entry.getValue();
			Field[] fields = bean.getClass().getDeclaredFields();
			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				if (field.getAnnotation(Autowired.class) != null) {    //Autowired注解的处理
					//字段是接口类型 设置默认接口的实现类
					try {
						setAutowiredField(bean, field);
					} catch (IllegalAccessException e) {
						log.error(e.getMessage(), e);
					}
				}
			}

		}
	}

	/**
	 * 设置自动注入的字段.
	 *
	 * @param bean
	 * @param field
	 */
	private void setAutowiredField(Object bean, Field field) throws IllegalAccessException {
		if (field.getType().isInterface()) {
			//通过bean的subClass来获取接口实现类bean
			for (Map.Entry<String, BeanDefinition> entry : this.getBeanDefinitionsMap().entrySet()) {
				String key = entry.getKey();
				BeanDefinition definition = entry.getValue();
				if (definition.getSubClass() != null && definition.getSubClass().getName().equals(field.getType().getName())) {
					field.setAccessible(true);
					field.set(bean, this.getBeanMap().get(key));
				}
			}
		} else {
			field.setAccessible(true);
			field.set(bean, this.getBeanMap().get(field.getName()));
		}
	}

	@Override
	public <T> T getBean(String beanName, Class<T> claz) {
		final Object bean = beanMap.get(beanName);
		if (bean == null) {
			throw new NullPointerException("No Fount Bean In BeanMap ");
		}
		return claz.cast(bean);
	}

	@Override
	public <T> T getBean(Class<T> claz) {
		Assert.notNull(claz, "Required type must not be null");
		for (Map.Entry<String, BeanDefinition> map : getBeanDefinitionsMap().entrySet()) {
			if (map.getValue().getBeanClass().equals(claz)) {
				return claz.cast(getBeanMap().get(map.getKey()));
			}
			if (claz.isInterface() && map.getValue().getSubClass() != null && map.getValue().getSubClass().equals(claz)) {
				return claz.cast(getBeanMap().get(map.getKey()));
			}
		}
		return null;
	}

	@Override
	public <T> T getBean(String beanName) {
		Assert.notNull(beanName, "Required beanName must not be null");
		return (T) beanMap.get(beanName);
	}

	@Override
	public Map<String, Object> getBeanMap() {
		return beanMap;
	}

	@Override
	public Map<String, BeanDefinition> getBeanDefinitionsMap() {
		return beanDefinitionsMap;
	}


	@Override
	public boolean containsBean(String beanName) {
		return this.getBeanMap().containsKey(beanName);
	}
}
