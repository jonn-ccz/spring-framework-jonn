package com.jonn.spring.framework.context.bean;

import cn.hutool.core.util.BooleanUtil;
import lombok.Data;

/**
 * 下午2:24 2020/9/9
 * lagou-transfer
 * Author: jonn
 * Desc:BeanDefinition.
 */
@Data
public class BeanDefinition {


	private String beanId;

	private Object bean;

	private Class<?> beanClass;

	/**
	 * 接口或父類.
	 */
	private Class<?> subClass;

	private Boolean isProxy;

	public Boolean getProxy() {
		return isProxy;
	}

	public void setProxy(Boolean proxy) {
		isProxy = proxy;
	}

	private Class<?> annotation;

	private String beanName;

	private String beanPackageName;

	public Object getBean() {
		return bean;
	}

	public void setBean(Object bean) {

		this.bean = bean;
	}

	public Class<?> getBeanClass() {
		return beanClass;
	}

	public void setBeanClass(Class<?> beanClass) {
		if(beanClass.getInterfaces().length >0){
		  this.subClass =	beanClass.getInterfaces()[0];
		}
		this.beanClass = beanClass;
	}

	public BeanDefinition() {

	}

	public BeanDefinition(String beanId, Object bean, Class<?> beanClass, Class<?> subClass, Class<?> annotation, String beanName, String beanPackageName) {
		if(beanClass.getInterfaces().length >0){
			this.subClass =	beanClass.getInterfaces()[0];
		}
		this.beanId = beanId;
		this.bean = bean;
		this.beanClass = beanClass;
		this.subClass = subClass;
		this.annotation = annotation;
		this.beanName = beanName;
		this.beanPackageName = beanPackageName;
	}

	public String getBeanId() {
		return beanId;
	}

	public void setBeanId(String beanId) {
		this.beanId = beanId;
	}


	public Class<?> getSubClass() {
		return subClass;
	}

	public void setSubClass(Class<?> subClass) {
		this.subClass = subClass;
	}

	public Class<?> getAnnotation() {
		return annotation;
	}

	public void setAnnotation(Class<?> annotation) {
		this.annotation = annotation;
	}

	public String getBeanName() {
		return beanName;
	}

	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	public String getBeanPackageName() {
		return beanPackageName;
	}

	public void setBeanPackageName(String beanPackageName) {
		this.beanPackageName = beanPackageName;
	}
}
