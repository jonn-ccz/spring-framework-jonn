package com.jonn.spring.framework.context.factory;

import cn.hutool.core.lang.Assert;
import com.jonn.spring.framework.context.builder.AnnotationClassBuilder;
import com.jonn.spring.framework.context.proxy.TranscationalProxyFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Map;

/**
 * 下午1:46 2020/9/8
 * Author: jonn
 * Desc:注解方式扫描包路径.
 */
@Slf4j
public class AnnotationConfigApplicationContext {


	private BeanFactory beanFactory;

	public AnnotationConfigApplicationContext(BeanFactory beanFactory,String... basePackages ) {
		//扫描包.
		this.beanFactory = beanFactory;
		this.scanPackages(basePackages);
	}

	/**
	 * 扫描包路径.
	 *
	 * @param basePackages
	 */
	private void scanPackages(String... basePackages) {
		Assert.notNull(basePackages, "scan package name can not null");
		for (int i = 0; i < basePackages.length; i++) {
			AnnotationClassBuilder annotationClassBuilder = new AnnotationClassBuilder(beanFactory, basePackages[i]);
		}
	}




}
