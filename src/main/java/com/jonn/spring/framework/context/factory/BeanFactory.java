package com.jonn.spring.framework.context.factory;

import com.jonn.spring.framework.context.bean.BeanDefinition;

import java.util.Map;

/**
 * 上午11:14 2020/9/8
 * lagou-transfer
 * Author: jonn
 * Desc:
 */
public interface BeanFactory {


	<T> T getBean(String beanName, Class<T> claz);

	<T> T getBean(Class<T> claz);

	<T> T getBean(String beanName);

	Map<String, Object> getBeanMap();

	Map<String, BeanDefinition> getBeanDefinitionsMap();

	boolean containsBean(String beanName);
}
