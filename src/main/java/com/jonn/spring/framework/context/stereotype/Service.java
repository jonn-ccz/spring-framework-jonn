package com.jonn.spring.framework.context.stereotype;

import java.lang.annotation.*;

/**
 * 上午10:54 2020/9/8
 * Author: jonn
 * Desc:
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.TYPE)
public @interface Service {
	String value() default "";
}
