package com.jonn.spring.framework.context.builder;

import cn.hutool.core.util.ReflectUtil;
import com.jonn.spring.framework.Constants;
import com.jonn.spring.framework.context.bean.BeanDefinition;
import com.jonn.spring.framework.context.factory.BeanFactory;
import com.jonn.spring.framework.context.factory.Transaction;
import com.jonn.spring.framework.context.proxy.TranscationalProxyFactory;
import com.jonn.spring.framework.context.stereotype.Component;
import com.jonn.spring.framework.context.stereotype.Service;
import com.jonn.spring.framework.context.stereotype.Transcational;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * 下午3:38 2020/9/8
 * spring-framework-jonn
 * Author: jonn
 * Desc:
 */
@Slf4j
public class AnnotationClassBuilder {


	private BeanFactory beanFactory;

	private String packageName;

	public AnnotationClassBuilder(BeanFactory beanFactory, String packageName) {
		this.beanFactory = beanFactory;
		this.packageName = packageName;
		getClasses();
	}

	private Map<String, Object> getBeanMap() {
		return beanFactory.getBeanMap();
	}

	private Map<String, BeanDefinition> getBeanClassMap() {
		return beanFactory.getBeanDefinitionsMap();
	}


	/**
	 * 获取某个包下的所有类
	 */
	public void getClasses() {
		Enumeration<URL> urls = null;
		try {
			urls = this.getClass().getClassLoader().getResources(packageName.replace(".", "/"));
			while (urls.hasMoreElements()) {
				URL url = urls.nextElement();
				if (url != null) {
					String protocol = url.getProtocol();
					if (protocol.equals("file")) {
						String packagePath = url.getPath();
						addClass(getBeanMap(), packagePath, packageName);
					}
				}
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}

	}

	private void addClass(Map<String, Object> beanMap, String packagePath, String packageName) {
		//
		File[] files = new File(packagePath).listFiles(new FileFilter() {
			@Override
			public boolean accept(File file) {
				return (file.isFile() && file.getName().endsWith(".class")) || file.isDirectory();
			}
		});
		for (File file : files) {
			String fileName = file.getName();
			if (file.isFile()) {
				String className = fileName.substring(0, fileName.lastIndexOf("."));
				if (StringUtils.isNotEmpty(packageName)) {
					className = packageName + "." + className;
				}
				doAddClass(beanMap, className);
			} else {
				String subPackagePath = fileName;
				if (StringUtils.isNotEmpty(packagePath)) {
					subPackagePath = packagePath + "/" + subPackagePath;
				}
				String subPackageName = fileName;
				if (StringUtils.isNotEmpty(packageName)) {
					subPackageName = packageName + "." + subPackageName;
				}
				addClass(beanMap, subPackagePath, subPackageName);
			}
		}
	}

	/**
	 * 加载类
	 */
	public static Class<?> loadClass(String className, boolean isInitialized) {
		Class<?> cls;
		try {
			cls = Class.forName(className);
			//如果为接口类型
			if (cls.isInterface()) {
				return null;
			}
		} catch (ClassNotFoundException e) {
			log.error("实例化接口失败");
			return null;
		}
		return cls;
	}

	/**
	 * 加载类（默认将初始化类）
	 */
	public static Class<?> loadClass(String className) {
		return loadClass(className, true);
	}

	/**
	 * 提取添加了注解的bean。
	 *
	 * @param className
	 */
	private void doAddClass(Map<String, Object> beanMap, String className) {
		Class<?> aClass = loadClass(className, true);
		try {
			if (aClass == null) {
				return;
			}
			//提取注解bean
			handleBean(beanMap, aClass);
		} catch (InstantiationException | IllegalAccessException e) {
			log.error(e.getMessage(), e);
		}
	}

	/**
	 * 处理注解bean.
	 *
	 * @param beanMap
	 * @param aClass
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	private void handleBean(Map<String, Object> beanMap, Class<?> aClass) throws InstantiationException, IllegalAccessException {
		synchronized (beanMap) {

			BeanDefinition beanDefinition = new BeanDefinition();
			String componentName = "";
			//实例化对象
			Object o = null;
			if (aClass.isAnnotationPresent(Component.class)) {
				componentName = aClass.getAnnotation(Component.class).value();
				beanDefinition.setAnnotation(Component.class);
				o = aClass.newInstance();
			} else if (aClass.isAnnotationPresent(Service.class)) {
				beanDefinition.setAnnotation(Service.class);
				componentName = aClass.getAnnotation(Service.class).value();
				o = aClass.newInstance();
			} else if (aClass.isAnnotationPresent(Transcational.class)) {
				beanDefinition.setAnnotation(Transcational.class);
				componentName = aClass.getAnnotation(Transcational.class).value();
				o = aClass.newInstance();
			}
			if (o == null) {
				return;
			}
			if (StringUtils.isBlank(componentName)) {
				componentName = (new StringBuilder()).append(Character.toLowerCase(aClass.getSimpleName().charAt(0))).append(aClass.getSimpleName().substring(1)).toString();
			}
			beanMap.put(componentName, o);
			beanDefinition.setBeanId(componentName);
			beanDefinition.setBeanClass(aClass);
			beanDefinition.setBean(o);
			beanDefinition.setBeanPackageName(aClass.getName());
			getBeanClassMap().put(componentName, beanDefinition);
		}
	}


	public static void main(String[] args) throws IOException {


	}
}
