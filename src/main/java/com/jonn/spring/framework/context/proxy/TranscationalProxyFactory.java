package com.jonn.spring.framework.context.proxy;

import com.jonn.spring.framework.context.factory.Transaction;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 下午3:17 2020/9/9
 * lagou-transfer
 * Author: jonn
 * Desc: 事务的代理工厂.
 */
@Slf4j
public class TranscationalProxyFactory implements InvocationHandler {


	private Transaction transaction;

	private Object target;


	public TranscationalProxyFactory(Transaction transaction, Object target) {
		this.transaction = transaction;
		this.target = target;
	}

	/**
	 * 创建JDK代理对象.
	 *
	 * @return
	 */
	public Object createJDKProxy() {
		return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), this);
	}

	/**
	 * 创建cglib动态代理.
	 *
	 * @return
	 */
	public Object createCglibProxy() {
		Enhancer enhancer = new Enhancer();
		//设置目标类的字节码文件
		enhancer.setSuperclass(target.getClass());
		//设置回调函数
		enhancer.setCallback(new MethodInterceptor() {
			@Override
			public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
				log.info("use cglib proxy");
				Object result = null;
				transaction.beginTransaction();
				try {
					result = methodProxy.invokeSuper(o, args);
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					transaction.rollback();
				}
				transaction.commit();
				return result;
			}
		});
		return enhancer.create();
	}


	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Object result = null;
		transaction.beginTransaction();
		try {
			result = method.invoke(target, args);
			transaction.commit();
		} catch (Exception e) {
			log.error("throw error rollback", e);
			transaction.rollback();
		}
		return result;
	}
}
