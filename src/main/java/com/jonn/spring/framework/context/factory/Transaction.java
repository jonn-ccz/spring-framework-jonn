package com.jonn.spring.framework.context.factory;

/**
 * 下午3:19 2020/9/9
 * lagou-transfer
 * Author: jonn
 * Desc:事务工厂.
 */
public interface Transaction {
	void beginTransaction();
	void commit();
	void rollback();
}
