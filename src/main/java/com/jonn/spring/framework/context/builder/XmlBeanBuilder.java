package com.jonn.spring.framework.context.builder;

import org.dom4j.Element;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * 下午1:30 2020/9/8
 * lagou-transfer
 * Author: jonn
 * Desc:
 */
public class XmlBeanBuilder {
	private Map<String, Object> beanMap;

	public XmlBeanBuilder(Map<String, Object> beanMap) {
		this.beanMap = beanMap;
	}
	/**
	 * 配置方式装载bean.
	 *
	 * @param root
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public void parseXMLBean(Element root) {
		List<Element> beansList = root.selectNodes("//bean");
		try {
			//读取bean
			for (int i = 0; i < beansList.size(); i++) {
				Element bean = beansList.get(i);
				String id = bean.attributeValue("id");
				String clazz = bean.attributeValue("class");
				Class<?> aClass = Class.forName(clazz);
				beanMap.put(id, aClass.newInstance());
			}
			//处理依赖关系
			List<Element> propertyList = root.selectNodes("//property");
			for (int i = 0; i < propertyList.size(); i++) {
				Element property = propertyList.get(i);
				String methodName = property.attributeValue("name");
				String ref = property.attributeValue("ref");
				Element parentElement = property.getParent();
				//父对象
				String parentId = parentElement.attributeValue("id");
				Object parent = beanMap.get(parentId);
				Method[] methods = parent.getClass().getMethods();
				for (Method method : methods) {
					if (method.getName().equalsIgnoreCase("set" + methodName)) {
						method.invoke(parent, beanMap.get(ref));
					}
				}
				beanMap.put(parentId, parent);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
